import React, { Component } from "react";
import Chart from "react-apexcharts";
import cx from "classnames";
import styles from "./Chart.module.css";

class Graph extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories: ["1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998"]
        }
      },
      series: [
        {
          name: "series-1",
          data: [30, 40, 45, 50, 49, 60, 70, 91]
        }
      ]
    };
  }


  componentDidMount() {
    fetch("https://hpb.health.gov.lk/api/get-current-statistical")
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data !=null) {
          const output = data.data.daily_pcr_testing_data.map(({ count, ...rest }) => ({
            ...rest,
            count: +count
          }));
          console.log(output);
          var groupedByWeek = output.reduce((m, o) => {
            var monday = getMonday(new Date(o.date));
            var mondayYMD = monday.toISOString().slice(0, 10);
            var found = m.find((e) => e.date === mondayYMD);
            if (found) {
              found.count += o.count;
            } else {
              o.date = mondayYMD;
              m.push(o);
            }
            return m;
          }, []);
    
          console.log(groupedByWeek);
    
          const count = groupedByWeek.map((a) => a.count);
          
          // this.setState(series, [{ name: "series-1", data: count }]);
          
          this.setState(prevState => {
            let series = Object.assign({}, prevState.series);  
            series = [{ name: "Count", data: count }];  
            
            return { series };                                
          })


          const week = groupedByWeek.map((a) => a.date);
          // this.setState(options.xaxis.categories , week);
          this.setState(prevState => {
            let options = Object.assign({}, prevState.options);  
            options = {
              chart: {
                id: "basic-bar"
              },
              xaxis: {
                categories: week
              }
            }; 
            console.log(options);
            return { options };                                
          })
          console.log(week);
          

        }
    
        function getMonday(d) {
          var day = d.getDay();
          var diff = d.getDate() - day + (day === 0 ? -6 : 1);
          return new Date(d.setDate(diff));
        }
      })
      .catch(console.log);
  }

  render() {
    


   
      return (


        
        <div className="app" className={(styles.card)}>
           <h3>Daily PCR testing</h3>
        <div className="row" className={(styles.container)}>
         
          <div className="mixed-chart">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="bar"
                width="1000"
                height="400"
            />
          </div>
        </div>
      </div>
      );
    }
  

  
}

export default Graph;